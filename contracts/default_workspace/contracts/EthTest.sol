// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

contract TestPool {
    mapping (address => uint256) public balanceOf;

    receive() external payable {
        _depositTo(msg.sender);
    }

    function depositTo(address _recipient) external payable {
        _depositTo(_recipient);
    }

    function withdrawTo(address _recipient, uint256 _amount) external {
        require(_amount != 0, "WAAA: Zero withdraw amount.");

        _burn(msg.sender, _amount);

        (bool _success, ) = _recipient.call{value: _amount}("");
        require(_success, "WAAA: Failed to transfer AAA.");
    }

    function _depositTo(address _recipient) internal {
        require(msg.value != 0, "WAAA: Zero deposit amount.");

        _mint(_recipient, msg.value);
    }

    function _mint(address _whom, uint256 _amount) internal {
        balanceOf[_whom] += _amount;
    }

    function _burn(address _whom, uint256 _amount) internal {
        balanceOf[_whom] -= _amount;
    }
}
