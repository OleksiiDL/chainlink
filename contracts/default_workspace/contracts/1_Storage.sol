// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */
contract TestContract {
    function getGroupIndexByNftNumber(uint256[] memory _counts, uint256 _num) external pure returns (uint256) {
        uint256 _index;

        for (uint256 _i; _i < _counts.length; _i++) {
            if (_counts[_i] < _num) continue;

            _index = _i;
            break;
        }

        return _index;
    }
}