// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

contract StablePool {
    address public assetAddr;
    bytes32 public assetKey;
    uint256 public annualBorrowRate;

    uint256 public someNum;

    function stablePoolInitialize(
        address _assetAddr,
        bytes32 _assetKey,
        uint256 _annualBorrowRate
    ) external {
        assetAddr = _assetAddr;
        assetKey = _assetKey;
        annualBorrowRate = _annualBorrowRate;
    }

    function getInitSelector() public pure returns (bytes4) {
        return this.stablePoolInitialize.selector;
    }

    function getInitData(
        address _assetAddr,
        bytes32 _assetKey,
        uint256 _annualBorrowRate
    ) public pure returns (bytes memory) {
        return abi.encode(_assetAddr, _assetKey, _annualBorrowRate);
    }

    function getEncodeWithSelectorData(
        address _assetAddr,
        bytes32 _assetKey,
        uint256 _annualBorrowRate
    ) public pure returns (bytes memory) {
        return abi.encodeWithSelector(getInitSelector(), _assetAddr, _assetKey, _annualBorrowRate);
    }

    function toAssetKey(uint256 _number) public pure returns (bytes32) {
        return bytes32(_number);
    }

    function getFuncSelector() public pure returns (bytes4) {
        return this.setNumber.selector;
    }

    function setNumber(uint256 _number) external {
        someNum = _number;
    }
}