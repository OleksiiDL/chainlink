// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

contract LiquidityPool {
    address public assetAddr;
    bytes32 public assetKey;
    string public symbol;

    function liquidityPoolInitialize(
        address _assetAddr,
        bytes32 _assetKey,
        string calldata _symbol
    ) external {
        assetAddr = _assetAddr;
        assetKey = _assetKey;
        symbol = _symbol;
    }

    function getInitSelector() public pure returns (bytes4) {
        return this.liquidityPoolInitialize.selector;
    }

    function getInitData(
        address _assetAddr,
        bytes32 _assetKey,
        string calldata _symbol
    ) public pure returns (bytes memory) {
        return abi.encode(_assetAddr, _assetKey, _symbol);
    }

    function getEncodeWithSelectorData(
        address _assetAddr,
        bytes32 _assetKey,
        string calldata _symbol
    ) public pure returns (bytes memory) {
        return abi.encodeWithSelector(getInitSelector(), _assetAddr, _assetKey, _symbol);
    }
}