// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

contract Factory {
    bytes4 public stableInitSelector;
    bytes4 public liquidityInitSelector;

    constructor(bytes4 _stableInitSelector, bytes4 _liquidityInitSelector) {
        stableInitSelector = _stableInitSelector;
        liquidityInitSelector = _liquidityInitSelector;
    }

    function setSelector(bytes4 _selector, bool _isStable) external {
        if (_isStable) {
            stableInitSelector = _selector;
        } else {
            liquidityInitSelector = _selector;
        }
    }

    function callInit(address _poolAddr, bytes memory _initData, bool _isStable) external {
        bytes4 _initSelector = _isStable ? stableInitSelector : liquidityInitSelector;

        _poolAddr.call(abi.encodePacked(_initSelector, _initData));
    }

    function buildData(bytes memory _data, bool _isStable) public view returns (bytes memory) {
        bytes4 _initSelector = _isStable ? stableInitSelector : liquidityInitSelector;

        return abi.encodeWithSelector(_initSelector, _data);
    }
}
