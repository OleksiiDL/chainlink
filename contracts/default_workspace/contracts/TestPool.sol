// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.3;

contract TestPool {
    uint256 public someNumber;
    address public someAddr;
    bytes32 public someBytes;

    function toAssetKey(uint256 _number) public pure returns (bytes32) {
        return bytes32(_number);
    }

    function getFuncSelector() public pure returns (bytes4) {
        return this.setValues.selector;
    }

    function setValues(uint256 _number, address _addr, bytes32 _key) external {
        someNumber = _number;
        someAddr = _addr;
        someBytes = _key;
    }

    function concatBytes(bytes memory _data) external pure returns (bytes memory) {
        return abi.encodePacked(getFuncSelector(), _data);
    }

    function buildData(uint256 _number, address _addr, bytes32 _key) external pure returns (bytes memory) {
        return abi.encode(_number, _addr, _key);
    }

    function buildFullData(uint256 _number, address _addr, bytes32 _key) external pure returns (bytes memory) {
        return abi.encodeWithSelector(getFuncSelector(), _number, _addr, _key);
    }
}
