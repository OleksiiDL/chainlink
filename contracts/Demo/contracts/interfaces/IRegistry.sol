// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.3;

/**
 * This is the main register of the system, which stores the addresses of all the necessary contracts of the system.
 * With this contract you can add new contracts, update the implementation of proxy contracts
 */
interface IRegistry {
    /// @notice The function to add new (non-proxy) contract to the registry
    /// @dev Only contract owner can call this function
    /// @param _name the assigned name to the contract
    /// @param _contractAddress the address of the contract to be added
    function addContract(bytes32 _name, address _contractAddress) external;

    /// @notice The function to add new (non-proxy) contract to the registry automatically deploying a
    /// TransparentUpgradeableProxy with <contractAddress> as a destination. This will enable the upgreadibility mechanism
    /// @dev Only contract owner can call this function
    /// @param _name the assigned name to the contract
    /// @param _contractAddress the implementation contract that will be used as a proxy's destination
    function addProxyContract(bytes32 _name, address _contractAddress) external;

    /// @notice The function to add new (proxy) contract to the registry. This will enable the upgreadibility mechanism
    /// @dev Only contract owner can call this function
    /// @param _name the assigned name to the contract
    /// @param _contractAddress the address of the proxy
    function justAddProxyContract(bytes32 _name, address _contractAddress) external;

    /// @notice The function to delete the existing contract from the registry
    /// @dev Only contract owner can call this function
    /// @param _name the name of the contract to be deleted
    function deleteContract(bytes32 _name) external;

    /// @notice The function to upgrade the implementation of the existing proxy contract
    /// @dev Only contract owner can call this function
    /// @param _name the name of the proxy contract to be upgraded
    /// @param _newImplementation the new implementation address
    function upgradeContract(bytes32 _name, address _newImplementation) external;

    /// @notice The function to upgrade the implemetation of the existing proxy contract with a function call
    /// @dev Only contract owner can call this function
    /// @param _name the name of the proxy to be upgraded
    /// @param _newImplementation the new implmenetation address
    /// @param _functionSignature the signature of the function to be called. \
    /// Only the functions with no parameters are supported. Ex: "someFunc()"
    function upgradeContractAndCall(
        bytes32 _name,
        address _newImplementation,
        string calldata _functionSignature
    ) external;

    /// @notice Utility function that is used to inject dependencies into the specified contract
    /// @dev Only contract owner can call this function
    /// @param _name the name of the contract
    function injectDependencies(bytes32 _name) external;

    /// @notice Function to get the address of the DefiCore contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return DefiCore contract address
    function getDefiCoreContract() external view returns (address);

    /// @notice Function to get the address of the SystemParameters contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return SystemParameters contract address
    function getSystemParametersContract() external view returns (address);

    /// @notice Function to get the address of the AssetParameters contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return AssetParameters contract address
    function getAssetParametersContract() external view returns (address);

    /// @notice Function to get the address of the RewardsDistribution contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return RewardsDistribution contract address
    function getRewardsDistributionContract() external view returns (address);

    /// @notice Function to get the address of the UserInfoRegistry contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return UserInfoRegistry contract address
    function getUserInfoRegistryContract() external view returns (address);

    /// @notice Function to get the address of the SystemPoolsRegistry contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return SystemPoolsRegistry contract address
    function getSystemPoolsRegistryContract() external view returns (address);

    /// @notice Function to get the address of the LiquidityPoolFactory contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return LiquidityPoolFactory contract address
    function getLiquidityPoolFactoryContract() external view returns (address);

    /// @notice Function to get the address of the PriceManager contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return PriceManager contract address
    function getPriceManagerContract() external view returns (address);

    /// @notice Function to get the address of the InterestRateLibrary contract
    /// @dev Used in dependency injection mechanism in the system
    /// @return InterestRateLibrary contract address
    function getInterestRateLibraryContract() external view returns (address);

    /// @notice Function to get the address of system rewards token
    /// @dev Used in dependency injection mechanism in the system
    /// @return contract address of the system rewards token
    function getRewardsTokenContract() external view returns (address);

    /// @notice Utility function to check if the given contract was added to the registry
    /// @param _name the name of the contract
    /// @return true if the contract was added, false otherwise
    function hasContract(bytes32 _name) external view returns (bool);

    /// @notice The function to fetch the upgrader contract. The upgrader contract is needed to overcome
    /// TransparentUpgreadableProxy's admin previliges and be able to inject dependencies
    /// @return the upgrader's address
    function getProxyUpgrader() external view returns (address);

    /// @notice The function to fetch the implementation of the given proxy contract
    /// @param _name the name of the existing proxy contract
    /// @return the address of the implementation to proxy points to
    function getImplementation(bytes32 _name) external view returns (address);

    /// @notice Utility function that is used to fetch the contract address by its name
    /// @param _name the name of the contract
    /// @return contract's address by its name
    function getContract(bytes32 _name) external view returns (address);
}
