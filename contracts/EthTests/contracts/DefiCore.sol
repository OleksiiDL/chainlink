// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.3;

import "./LiquidityPool.sol";
import "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol";

contract DefiCore {

    function addLiquidity(address _liquidityPool, uint256 _amountToDeposit) external payable {
        LiquidityPool(_liquidityPool).addLiquidity{value: msg.value}(msg.sender, _amountToDeposit);
    }
}
