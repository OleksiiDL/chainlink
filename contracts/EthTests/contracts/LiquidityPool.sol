// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.3;

import "./WAAA.sol";

contract LiquidityPool {
    WAAA public nativeToken;

    mapping (address => uint256) public userBalances;

    constructor(address payable _nativeToken) {
        nativeToken = WAAA(_nativeToken);
    }

    function addLiquidity(address _userAddr, uint256 _amountToDeposit) external payable {
        uint256 _userTokenBalance = nativeToken.balanceOf(_userAddr);

        if (_amountToDeposit > _userTokenBalance) {
            uint256 _toDepostAmount = _amountToDeposit - _userTokenBalance;

            require(msg.value == _toDepostAmount, "Incorrect curreny amount.");

            nativeToken.depositTo{value: _toDepostAmount}(_userAddr);
        }

        nativeToken.transferFrom(_userAddr, address(this), _amountToDeposit);

        userBalances[_userAddr] += _amountToDeposit;
    }

    function getNativeBalance(address _toCheckAddr) external view returns (uint256) {
        return _toCheckAddr.balance;
    }
}
