// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";

contract WAAA is ERC20Permit {
    constructor() ERC20Permit("Wrapped AAA") ERC20("Wrapped AAA", "WAAA") {}

    receive() external payable {
        _depositTo(msg.sender);
    }

    function depositTo(address _recipient) external payable {
        _depositTo(_recipient);
    }

    function withdrawTo(address _recipient, uint256 _amount) external {
        require(_amount != 0, "WAAA: Zero withdraw amount.");

        _burn(msg.sender, _amount);

        (bool _success, ) = _recipient.call{value: _amount}("");
        require(_success, "WAAA: Failed to transfer AAA.");
    }

    function _depositTo(address _recipient) internal {
        require(msg.value != 0, "WAAA: Zero deposit amount.");

        _mint(_recipient, msg.value);
    }
}
